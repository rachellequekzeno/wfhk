<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package Understrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' ); ?>

<div class="wrapper p-0 the-office-hero">
	<div class="container text-center position-relative title-header-section">
		<h1 class="text-white position-absolute center text-uppercase header-title"> <?php the_title(); ?> </h1>
		<div class="scroll-more position-absolute">
			<div class="d-flex justify-content-center">
				<lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
			</div>
			<div class="pt-3 scroll-more-text transat-text-black text-white"> Scroll to <br> explore more</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="w-100" style="height: 61px"></div>
		</div>
		<div class="row mt-5 justify-content-center next-content-to-scroll">
			<div class="col-md-8">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
