<?php
/**
* Template Name: Page The Office
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="wrapper p-0 the-office-hero">
    <div class="container text-center position-relative title-header-section">
        <h1 class="text-white position-absolute center text-uppercase header-title">
            The Workspace
        </h1>
        <div class="scroll-more position-absolute">
            <div class="d-flex justify-content-center">
                <lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
            </div>
            <div class="pt-3 scroll-more-text transat-text-black">
                Scroll to <br>
                explore more
            </div>
        </div>
    </div>
    <div class="wrapper island-at-back">
        <div class="container next-content-to-scroll">
            <div class="row m-0 mobile-hide">
                <div class="w-100" style="height: 61px"></div>
            </div>

            <div class="row mt-5 justify-content-center">
                <div class="col-md-5 line-height-25px align-items-start community-image-sections d-flex justify-content-end">
                    <div class="position-relative">
                        <img src="<?php echo get_template_directory_uri(); ?>-child/images/Image-Office.png" alt="Image Office">
                    </div>
                </div>
                <div class="col-md-5 line-height-25px ml-lg-5 ml-md-5 ml-0 pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px">
                        Robinson Crusoe has been cut off from the outside world. Until now. Together with Lenovo they have upgraded the internet, providing the locals with better education, improved connectivity to their families on the mainland, and enhanced weather reporting.
                        <br><br>
                        It also means that our applicants can do their 9-5 no problem. Chat online, check emails, attend meetings that should have been an email…no blaming bad connection I’m afraid. But this project is about investing in the local community. So, whilst the visitors will have access to the latest Lenovo technology – from PCs to tablets, monitors to smartphones – the locals will be able to use it forever. 
                    </div>
                </div>
            </div>
            <div class="row m-0 mobile-hide">
                <div class="w-100" style="height: 61px"></div>
            </div>

            <div class="row mt-5 justify-content-center align-items-center">
                <div class="col-md-3 line-height-25px">
                    <div class="text-white pr-1 pt-3 font-14px text-center">
                        <h5 class="text-white">P11 Pro Tablet </h5>
                        <br>
                        “line to go here once approved”
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px text-center">
                        <h5 class="text-white">ThinkBook 15p Gen 1 20TN002LUK </h5>
                        <br>
                        “line to go here once approved”
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px text-center">
                        <h5 class="text-white">ThinkBook 16P 20YM002UUK </h5>
                        <br>
                        “line to go here once approved”
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px text-center">
                        <h5 class="text-white">X1 Nano 20UN002EUK </h5>
                        <br>
                        “line to go here once approved”
                    </div>
                </div>
            </div>
            <div class="row mt-5 justify-content-center align-items-center">
                <div class="col-md-3 line-height-25px">
                    <div class="text-white pr-1 pt-3 font-14px text-center">
                        <h5 class="text-white">X1 Titanium 20QA001HUK </h5>
                        <br>
                        “line to go here once approved”
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px text-center">
                        <h5 class="text-white">Yoga 7 Pro</h5>
                        <br>
                        “line to go here once approved”
                    </div>
                </div>
            </div>

            <div class="row mt-5 justify-content-center">
                <div class="col-md-8">
                    <div class="text-white px-5 py-3 text-center">
                        This may be the remotest office on earth but you won’t be cut off from the world (unless that’s what you want).  Lenovo has installed internet on the island and you’ll be kitted out with the latest Lenovo devices: ThinkPads, Yogas, plus 24 hour tech support. 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php get_footer(); ?>
