<?php
/**
* Template Name: Page Island Conservation
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="wrapper p-0 island-conservation-hero">
    <div class="container text-center position-relative title-header-section">
        <h1 class="text-white position-absolute center text-uppercase header-title">
            Island Conservation
        </h1>
        <div class="scroll-more position-absolute">
            <div class="d-flex justify-content-center">
                <lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
            </div>
            <div class="pt-3 scroll-more-text transat-text-black">
                Scroll to <br>
                explore more
            </div>
        </div>
    </div>
    <div class="wrapper island-at-back next-content-to-scroll">
        <div class="container">

            <div class="row mt-5 justify-content-center align-items-center">
                <div class="col-md-5 order-md-1 order-2 line-height-25px mr-lg-5 mr-md-5 mr-0 pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px color-333333">
                        The International Union for Conservation of Nature, BirdLife International, the Alliance for Zero Extinction, and the World Wildlife Fund highlight the islands of the Juan Fernández Archipelago as one of the most ecologically vulnerable ecosystems in the world. The islands are sixty-one times richer in endemic plant species per square kilometer and thirteen times greater in endemic bird richness than the Galápagos. Few people live here, largely fishermen and their families: only 700 people live on Robinson Crusoe; 30 seasonal lobster fishermen visit Alejandro Selkirk a year.
                    </div>
                </div>
                <div class="col-md-5 order-md-2 order-1 line-height-25px island-image">
                    <div class="d-flex justify-content-center">
                        <div class="position-relative">
                            <img src="<?php echo get_template_directory_uri(); ?>-child/images/Island-Outline-Dark-Brown.png" alt="Robinson Crusoe">
                            <div class="position-absolute text-uppercase color-867066 font-25px robinson-text">
                                Robinson Crusoe
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-0 mobile-hide">
                <div class="w-100" style="height: 61px"></div>
            </div>

            <div class="row mt-5 justify-content-center align-items-center pt-lg-0 pt-md-0 pt-5">
                <div class="col-md-5 line-height-25px community-image-sections d-flex justify-content-center">
                    <div class="not-image-rectangle">
                        <div class="position-relative">
                            <img src="<?php echo get_template_directory_uri(); ?>-child/images/Image-Seals.png" alt="Seal Image">
                        </div>
                    </div>
                </div>
                <div class="col-md-5 line-height-25px ml-lg-5 ml-md-5 ml-0 pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px color-333333">
                        Invasive, non-native goats, rabbits, coatis, feral cats, mice, and rats present on the islands are destroying native plant and animal populations. Island Conservation and our partners are working to remove invasive species from the Juan Fernández Archipelago. Along with Chilean resource agencies, Island Conservation is conducting environmental education and community-based activities to gain local support for the restoration of these biologically important islands.
                    </div>
                </div>
            </div>
            <div class="row m-0">
                <div class="w-100" style="height: 61px"></div>
            </div>

            <div class="row mt-5 justify-content-center align-items-center">
                <div class="col-md-5 order-md-1 order-2 line-height-25px mr-lg-5 mr-md-5 mr-0 pt-lg-0 pt-md-0 pt-5">
                    <div class="text-white pr-1 pt-3 font-14px color-333333">
                        The International Union for Conservation of Nature, BirdLife International, the Alliance for Zero Extinction, and the World Wildlife Fund highlight the islands of the Juan Fernández Archipelago as one of the most ecologically vulnerable ecosystems in the world. The islands are sixty-one times richer in endemic plant species per square kilometer and thirteen times greater in endemic bird richness than the Galápagos. Few people live here, largely fishermen and their families: only 700 people live on Robinson Crusoe; 30 seasonal lobster fishermen visit Alejandro Selkirk a year.
                    </div>
                </div>
                <div class="col-md-5 order-md-2 order-1 line-height-25px community-image-sections d-flex justify-content-center">
                    <div class="not-image-rectangle">
                        <div class="position-relative">
                            <img src="<?php echo get_template_directory_uri(); ?>-child/images/Community-Boats.png" alt="Community Boats">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row m-0">
            <div class="w-100" style="height: 61px"></div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
