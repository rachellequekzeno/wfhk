<?php
/**
* Template Name: Page The Community
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="wrapper p-0 the-community-hero">
    <div class="container text-center position-relative title-header-section">
        <h1 class="text-white position-absolute center text-uppercase header-title">
            Community
        </h1>
        <div class="scroll-more position-absolute">
            <div class="d-flex justify-content-center">
                <lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
            </div>
            <div class="pt-3 scroll-more-text transat-text-black">
                Scroll to <br>
                explore more
            </div>
        </div>
    </div>
    <div class="container text-center next-content-to-scroll">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="text-white px-5 py-3 transat-text-medium" style="font-size: 42px;">
                    Island living is easy, relaxing, and slow right?
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper island-at-back">
        <div class="container">
            <div class="row m-0">
                <div class="w-100" style="height: 61px"></div>
            </div>
            <div class="row mt-5 justify-content-center align-items-center">
                <div class="col-md-5 line-height-25px community-image-sections position-relative d-flex justify-content-end">
                    <div class="position-relative">
                        <img src="<?php echo get_template_directory_uri(); ?>-child/images/Community-Child-Man.png" alt="Community Child Man">
                    </div>
                </div>
                <div class="col-md-5 line-height-25px ml-lg-5 ml-md-5 ml-0">
                    <div class="text-white pr-1 pt-3 font-14px">
                    You’ll get a funny look if you ask a local that. They will tell you that Robinson Crusoe is one of the most diverse environments on earth. It moves, changes, and shifts hourly. And that means you must react. Fast. 
                    <br><br>
                    The weather turns in a heartbeat. Endangered species are becoming extinct before our eyes. Plastic pollution is choking the island. Add in a lack of natural disaster emergency alerts, minimal healthcare, and climate change. It’s anything but slow on Robinson Crusoe. 
                    </div>
                </div>
            </div>
            <div class="row m-0 mobile-hide">
                <div class="w-100" style="height: 61px"></div>
            </div>

            <div class="row mt-5 justify-content-center align-items-center">
                <div class="col-md-5 order-md-1 order-2 line-height-25px mr-lg-5 mr-md-5 mr-0">
                    <div class="text-white pr-1 pt-3 font-14px">
                        But this small island of 900 people has big plans. They are working to protect their home and achieve a vision of a sustainable archipelago. One that promotes self-sufficiency, economic resilience, and conservation of their natural and cultural heritage. That’s why Lenovo's Intelligent Devices Group are the perfect partner. They understand the need for reactive, innovative technological solutions that can make a genuine difference to community-led projects. 
                    </div>
                </div>
                <div class="col-md-5 line-height-25px order-md-2 order-1 community-image-sections position-relative">
                    <div class="position-relative">
                        <img src="<?php echo get_template_directory_uri(); ?>-child/images/Community-Boats.png" alt="Community Boats">
                    </div>
                </div>
            </div>

            <div class="row m-0 mobile-hide">
                <div class="w-100" style="height: 61px"></div>
            </div>

            <div class="row mt-5 justify-content-center">
                <div class="col-md-5 line-height-25px align-items-baseline community-image-sections position-relative d-flex justify-content-end">
                    <div class="position-relative">
                        <img src="<?php echo get_template_directory_uri(); ?>-child/images/Community-Child-Man.png" alt="Community Boats">
                    </div>
                </div>
                <div class="col-md-5 line-height-25px ml-lg-5 ml-md-5 ml-0">
                    <div class="text-white pr-1 pt-3 font-14px">
                        Lenovo IDG’s first mission was to upgrade the internet for better connectivity. This increase in gigabytes means progress toward achieving the island’s sustainability goals. It could also be the difference between receiving a storm warning and not. Saving a species or extinction. 
                        <br><br>
                        Lenovo has upgraded the devices too. The community workspace will house Lenovo IDG’s smarter equipment, which is available for our lucky applicants to use whilst on the island. More importantly, it will serve the Robinson Crusoe community for years to come.
                        <br><br>
                        This project brings Lenovo’s vision of leading and enabling Smarter Technology for All, leaving a legacy that not only preserves this rich culture but helps it thrive in the future. 
                    </div>
                </div>
            </div>

        </div>
        <div class="row m-0">
            <div class="w-100" style="height: 61px"></div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
