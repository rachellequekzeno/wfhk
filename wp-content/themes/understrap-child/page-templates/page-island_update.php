<?php
/**
* Template Name: Page Island Update
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="wrapper p-0 island-update-hero">
	<div class="container text-center position-relative title-island-update-header-section">
		<div class="row">
			<div class="w-100" style="height: 225px"></div>
		</div>
		<h1 class="text-white text-uppercase header-title pull-up-title">
			Island Update
		</h1>
	</div>
	<div class="wrapper island-at-back">
		<div class="container-lg">
			<div class="row align-items-center pb-5 year-mpra remove-negative-margin-on-mobile">
				<div class="order-md-1 order-1 offset-md-0 offset-2 col-md-5 col-10 py-md-5 py-0 d-flex justify-content-lg-end justify-content-md-end justify-content-start remove-extra-margin-on-mobile">
					<div class="img-awards_year object-fit-cover object-center mb-md-3 mb-5 community-image-sections position-relative">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/Image-Office.png" alt="Hey">
						</div>
					</div>
				</div>
				<div class="order-md-2 order-2 corder-md-2 col-1 py-5 pull-timeline-dot">
					<div class="outer-dots"><div class="dots"></div></div>
					<div class="vl"></div>
				</div>
				<div class="order-md-3 order-3 col-md-5 col-10 text-left ">
					<div id="accordion" class="pb-lg-0 pb-md-0 pb-4">
						<div class="font-14px transat-text-medium">
							24th Jan 2022
						</div>
						<h2 class="mb-0 text-uppercase font-25px transat-text-standard pt-2">
							This is a right title with an update
						</h2>
						<div class="mt-4 body-text-accordion">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
						</div>
						<div class="card">
							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
								<div class="card-body p-0 mt-2">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</div>
							</div>
							<div class="card-header p-0 position-relative" id="headingOne">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed font-14px text-underline gotham-bold p-0 mt-3" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
										Show more
									</button>
								</h5>
							</div>

						</div>
					</div>
				</div>
			</div>

			<!-- Left side -->
			<div class="row align-items-center pb-5 year-mpra remove-negative-margin-on-mobile">
				<div class="order-md-1 order-3 col-md-5 col-10 text-md-right text-left ">
					<div id="accordion" class="pb-lg-0 pb-md-0 pb-4">
						<div class="font-14px transat-text-medium">
							24th Jan 2022
						</div>
						<h2 class="mb-0 text-uppercase font-25px transat-text-standard pt-2">
							This is a left title with an update
						</h2>
						<div class="mt-4 body-text-accordion">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
						</div>
						<div class="card">
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
								<div class="card-body p-0 mt-2">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</div>
							</div>
							<div class="card-header p-0 position-relative" id="headingTwo">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed font-14px text-underline gotham-bold p-0 mt-3" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Show more
									</button>
								</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="order-md-2 order-2 col-1 py-5 pull-timeline-dot left-content-dot-mobile">
					<div class="outer-dots"><div class="dots"></div></div>
					<div class="vl"></div>
				</div>
				<div class="order-md-3 order-1 offset-md-0 offset-2 col-md-5 col-10 py-md-5 py-0 justify-content-end  remove-extra-margin-on-mobile">
					<div class="img-awards_year object-fit-cover object-center mb-md-3 mb-5 community-image-sections position-relative">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/Image-Office.png" alt="Hey">
						</div>
					</div>
				</div>
			</div>


			<div class="row align-items-center pb-5 year-mpra remove-negative-margin-on-mobile">
				<div class="order-md-1 order-1 offset-md-0 offset-2 col-md-5 col-10 py-md-5 py-0 d-flex justify-content-lg-end justify-content-md-end justify-content-start remove-extra-margin-on-mobile">
					<div class="img-awards_year object-fit-cover object-center mb-md-3 mb-5 community-image-sections position-relative">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/Image-Office.png" alt="Hey">
						</div>
					</div>
				</div>
				<div class="order-md-2 order-2 corder-md-2 col-1 py-5 pull-timeline-dot">
					<div class="outer-dots"><div class="dots"></div></div>
					<div class="vl"></div>
				</div>
				<div class="order-md-3 order-3 col-md-5 col-10 text-left ">
					<div id="accordion" class="pb-lg-0 pb-md-0 pb-4">
						<div class="font-14px transat-text-medium">
							24th Jan 2022
						</div>
						<h2 class="mb-0 text-uppercase font-25px transat-text-standard pt-2">
							This is a right title with an update
						</h2>
						<div class="mt-4 body-text-accordion">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
						</div>
						<div class="card">
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
								<div class="card-body p-0 mt-2">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</div>
							</div>
							<div class="card-header p-0 position-relative" id="headingThree">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed font-14px text-underline gotham-bold p-0 mt-3" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Show more
									</button>
								</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
