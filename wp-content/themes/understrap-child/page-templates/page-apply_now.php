<?php
/**
* Template Name: Page Apply Now
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="position-relative">
    <div class="position-fixed apply-now-hero"></div>
    <div class="wrapper">
        <div class="wrapper">
            <div class="container-lg">
                <div class="row">
                    <div class="w-100" style="height: 80px;"></div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="sticky-top" style="top: 30px;">
                            <div class="transat-text-black text-uppercase font-50px" style="line-height: 53px;">
                                Work from anywhere…
                            </div>
                            <div class="row">
                                <div class="w-100" style="height: 20px;"></div>
                            </div>
                            <div class="font-12px">
                                Lenovo’s Work for Humankind project is working in partnership with the NGO Island Conservation and the infamous Robinson Crusoe island.​
                                <br><br>
                                Together, we’re looking for 8* people to continue working in their current jobs remotely, from Lenovo’s workspace on Robinson Crusoe Island. We need a range of different skills and specialties; the only thing we absolutely require is that you’re able to work remotely for 1 whole month from Day/Month/Year to Day/Month/Year.**​
                                <br><br>
                                If chosen, you’ll get to live on one of the remotest places on earth. What’s more, we’ll cover your flights, accommodation and living. All we ask in return is that, after work hours, you use your knowledge and/or skills to help support the local community and endangered wildlife.  Not a bad trade!​
                                <br><br>
                                The applicants will be chosen by Lenovo IDG and will be notified by Day/Month/Year**.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="position-relative w-100 h-100">
                            <img src="<?php echo get_template_directory_uri(); ?>-child/images/Paper-Top.png" alt="form-background" class="w-100 position-absolute form-image-top">
                            <div class="form-content-bg position-absolute"></div>
                            <div class="paper-form-page text-333333 w-75 m-auto position-relative" style="margin-left: 65px; z-index: 2">
                                <?php the_content(); ?>
                                <?php //echo do_shortcode('[contact-form-7 id="9" title="Apply Now"]'); ?>
                            </div>
                            <img src="<?php echo get_template_directory_uri(); ?>-child/images/Paper-Bottom.png" alt="form-background-bottom" class="w-100 form-image-bottom position-absolute">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
