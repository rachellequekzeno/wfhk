<?php
/**
* Template Name: Homepage
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="full-loading-bg position-fixed">
	<p id="loading-journey" class="position-absolute text-center">Loading journey<span>.</span><span>.</span><span>.</span></p>
	<lottie-player src="https://assets4.lottiefiles.com/packages/lf20_nxsief8g.json" class="loading-svg mx-auto w-100" background="transparent" speed="1" loop autoplay></lottie-player>
</div>

<section id="section1" class="first panel d-flex align-items-center justify-content-center">
	<div id="videoChapter1" class="video-chapter position-fixed h-100 w-100"><video muted autoplay playsinline class="h-100 w-100"><source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-1.mp4" type="video/mp4"></video></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="texting-word px-lg-0 px-4">
						<h2 class="h3 text-center text-uppercase text-white">
							<span>The remotest <span class="text-nowrap">workspace on earth</span> </span>
						</h2>
						<h1 class="text-center text-uppercase text-white">
							<span>Robinson Crusoe Island, Chile.</span>
						</h1>
						<div class="text-center">
							<a href="<?php bloginfo('url'); ?>/apply-now" class="btn btn-outline-white btn-apply rounded-0 px-lg-5 px-4 py-lg-3 py-2 text-white">Apply Now</a>
						</div>
					</div>
				</div>
			</div>
			<div class="scroll-more fixed-bottom">
				<div class="d-flex justify-content-center">
					<lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
				</div>
				<div class="scroll-more-text pt-3 pb-4 text-center transat-text-black text-white">
					Scroll to <br>
					explore more
				</div>
			</div>
		</div>


	</section>
	<section id="section2" class="panel d-flex align-items-center justify-content-start">
		<div id="videoChapter2" class="video-chapter position-fixed h-100 w-100"><video muted autoplay playsinline class="h-100 w-100"><source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-2.mp4" type="video/mp4"></video></div>
			<div class="container texting-word">
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="animate-box text-md-left text-center text-uppercase text-white wording-fade px-lg-0 px-5" data-wording-fade-order="0">
							<span>We’re Changing How</span><br class="d-md-block d-none" />
							<span>People Work <span class="text-nowrap font-italic">For Good</span></span>
						</h2>
						<p class="animate-box text-md-left text-center text-white wording-fade px-lg-0 px-5" data-wording-fade-order="1">
							<span>That means working from anywhere and </span><br class="d-md-block d-none" />
							<span>looking after the planet too.</span>
						</p>
					</div>
				</div>
			</div>
			<div class="scroll-more fixed-bottom">
				<div class="d-flex justify-content-center">
					<lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
				</div>
				<div class="scroll-more-text pt-3 pb-4 text-center transat-text-black text-white">
					Scroll to <br>
					explore more
				</div>
			</div>
		</section>
		<section id="section3" class="panel d-flex align-items-center justify-content-center">
			<div id="videoChapter3" class="video-chapter position-fixed h-100 w-100"><video muted autoplay playsinline class="h-100 w-100"><source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-3.mp4" type="video/mp4"></video></div>
				<div class="container texting-word">
					<div class="row">
						<div class="col-xl-9 col-lg-8">
							<h2 class="animate-box text-md-left text-center text-uppercase text-white wording-fade px-lg-0 px-4" data-wording-fade-order="0" >
								<span>Lenovo’s Intelligent Devices</span>
								<span>Group has built a workspace with</span>
								<span>smarter technology <span class="text-nowrap font-italic">for all.</span></span>
							</h2>
							<p class="animate-box text-md-left text-center text-white wording-fade px-lg-0 px-5" data-wording-fade-order="1" >
								<span>So, you can do your job full time.</span><br class="d-md-block d-none" />
								<span>Part time. Whatever time.</span>
							</p>
						</div>
					</div>
				</div>
				<div class="scroll-more fixed-bottom">
					<div class="d-flex justify-content-center">
						<lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
					</div>
					<div class="scroll-more-text pt-3 pb-4 text-center transat-text-black text-white">
						Scroll to <br>
						explore more
					</div>
				</div>
			</section>
			<section id="section4" class="panel d-flex align-items-center justify-content-center">
				<div id="videoChapter4" class="video-chapter position-fixed h-100 w-100"><video muted autoplay playsinline class="h-100 w-100"><source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-4.mp4" type="video/mp4"></video></div>
					<div class="container texting-word">
						<div class="row">
							<div class="col-lg-7">
								<h2 class="animate-box text-md-left text-center text-uppercase text-white wording-fade px-lg-0 px-4" data-wording-fade-order="0">
									<span>And you can help save a</span>
									<span>species in your free time.</span>
								</h2>
								<p class="animate-box text-md-left text-center text-white wording-fade px-lg-0 px-5" data-wording-fade-order="1">
									<span>Working in partnership with the NGO Island</span><br class="d-md-block d-none" />
									<span> Conservation and the local community. </span>
								</p>
							</div>
						</div>
					</div>
					<div class="scroll-more fixed-bottom">
						<div class="d-flex justify-content-center">
							<lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
						</div>
						<div class="scroll-more-text pt-3 pb-4 text-center transat-text-black text-white">
							Scroll to <br>
							explore more
						</div>
					</div>
				</section>
				<section id="section5" class="panel d-flex align-items-center justify-content-center">
					<div id="videoChapter5" class="video-chapter position-fixed h-100 w-100"><video muted autoplay playsinline class="h-100 w-100"><source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-5.mp4" type="video/mp4"></video></div>
						<div class="container texting-word">
							<div class="row justify-content-center">
								<div class="col-xl-5 col-lg-6">
									<h2 class="animate-box text-center text-white wording-fade" data-wording-fade-order="0">
										<span>APPLICATION</span><br class="d-md-block d-none" />
									</h2>
									<div class="animate-box mb-lg-5 mb-3 px-lg-0 px-5 wording-fade" data-wording-fade-order="1">
										<p class="text-center text-white">
											We need people to work on Robinson Crusoe. And we’ll require a wide range of skills, perspectives, and backgrounds.
										</p>
										<p class="text-center text-white">
											So, if you want to work (extremely) remotely on a legendary island, support a local community, and help save an endangered species…now’s your chance.
										</p>
									</div>
									<div class="animate-box text-center wording-fade" data-wording-fade-order="2">
										<a href="<?php bloginfo('url'); ?>/apply-now" class="btn btn-outline-white btn-apply rounded-0 px-5 py-3 text-white" style="">Apply Now</a>
									</div>
								</div>
							</div>
						</div>
						<div class="footer-section">
							<?php get_template_part( 'loop-templates/content', 'footer' ); ?>
						</div>
					</section>
					<nav class="position-fixed slider-navigation">
						<button class="d-block activates" data-section="0"></button>
						<button class="d-block" data-section="1"></button>
						<button class="d-block" data-section="2"></button>
						<button class="d-block" data-section="3"></button>
						<button class="d-block" data-section="4"></button>
					</nav>
				</div>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollTrigger.min.js"></script>
				<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/ScrollToPlugin3.min.js"></script>
				<?php get_footer(); ?>
