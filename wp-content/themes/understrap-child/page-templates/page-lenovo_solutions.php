<?php
/**
* Template Name: Page Lenovo Solutions
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="wrapper p-0 lenovo-solutions-hero">
    <div class="container text-center position-relative title-header-section">
        <h1 class="text-white position-absolute center text-uppercase header-title">
            Lenovo Solutions
        </h1>
        <div class="scroll-more position-absolute">
            <div class="d-flex justify-content-center">
                <lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
            </div>
            <div class="pt-3 scroll-more-text transat-text-black">
                Scroll to <br>
                explore more
            </div>
        </div>
    </div>
    <div class="container text-center next-content-to-scroll">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="text-white px-lg-5 px-md-5 px-0 py-3 line-height-25px font-14px">
                    Lenovo pushes technology to bring that vision to life. Today, this means developing smarter technology for all, tailored for people across political, socio-economic, and cultural backgrounds. Far beyond an unparalleled range of innovative devices, Lenovo creates services and solutions to radically transform how we live, learn, work, and play. Beyond our technology, we help build a smarter tomorrow by leading with our own values, shaped by a global community of diverse and conscientious employees and customers.
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="w-100" style="height: 61px"></div>
        </div>
        <div class="row mt-5">
            <div class="col-md-3 line-height-25px">
                <div class="text-center">
                    <h3 class="text-white transat-text-standard">
                        Education
                    </h3>
                </div>
                <div class="text-white pr-1 pt-3">
                    <p class="font-14px">From remote-ready tablets and PCs to <a href="https://www.lenovo.com/vrclassroom" target="_blank" rel="noopener">VR-powered field trips</a>, Lenovo opens the world to the next generation of innovators. And we partner with teams around the world to provide <a href="https://www.lenovo.com/us/en/about/foundation/" target="_blank" rel="noopener">access and opportunity to disadvantaged students</a>.</p>
                    <br>
                    <a href="https://techtoday.lenovo.com/us/en/solutions/education" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                </div>
            </div>
            <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                <div class="text-center">
                    <h3 class="text-white transat-text-standard">
                        Healthcare
                    </h3>
                </div>
                <div class="text-white pr-1 pt-3">
                    <p class="font-14px">Lenovo transforms medical care with dedicated devices, <a href="https://techtoday.lenovo.com/us/en/solutions/healthcare-lenovo-virtual-care" target="_blank" rel="noopener">virtual care solutions</a>, entire <a href="https://news.lenovo.com/lenovo-smart-medical-makes-care-more-accessible/" target="_blank" rel="noopener">hospital systems empowering patients</a>, and <a href="https://www.lenovo.com/in/en/smarter/human-centered-tech-for-better-healthcare/smart-technology-helps-cancer-treatment/" target="_blank" rel="noopener">AI-enhanced diagnostic research</a>.</p>
                    <br>
                    <p class="font-14px"><a href="https://techtoday.lenovo.com/us/en/solutions/healthcare" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-center">
                        <h3 class="text-white transat-text-standard">
                            Industry
                        </h3>
                    </div>
                    <div class="text-white pr-1 pt-3">
                        <p class="font-14px">An agile and adaptable <a href="https://news.lenovo.com/pressroom/press-releases/lenovo-ranked-16-in-gartners-2021-top-25-global-supply-chain/" target="_blank" rel="noopener">global supply chain</a> provides cities with the technology they need most, while <a href="https://news.lenovo.com/lenovo-cut-pc-production-planning-from-six-hours-to-90-seconds-artificial-intelligence/">artificial intelligence and robotics</a> create new, efficient manufacturing processes.</p>
                        <br>
                        <a href="https://techtoday.lenovo.com/us/en/solutions/smb/manufacturing" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-center">
                        <h3 class="text-white transat-text-standard">
                            Workplace
                        </h3>
                    </div>
                    <div class="text-white pr-1 pt-3">
                        <p class="font-14px">Suites of <a href="https://news.lenovo.com/pressroom/press-releases/lenovo-introduces-one-stop-iot-solution-for-smart-office-buildings-in-china/" target="_blank" rel="noopener">integrated devices and IoT technologies</a> offer personalized, secure, and seamless experiences for collaborative, <a href="https://news.lenovo.com/pressroom/press-releases/ride-the-remote-revolution-to-future-proof-your-business/" target="_blank" rel="noopener">work-from-anywhere employees</a>.</p>
                        <br>
                        <a href="https://techtoday.lenovo.com/us/en/solutions/smart-workplace-solutions" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="row mobile-hide">
                <div class="w-100" style="height: 61px"></div>
            </div>
            <div class="row">
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-center">
                        <h3 class="text-white transat-text-standard">
                            Entertainment
                        </h3>
                    </div>
                    <div class="text-white pr-1 pt-3">
                        <p class="font-14px">Lenovo technology empowers <a href="https://www.lenovo.com/us/en/data-center/solutions/customer-stories/dreamworks" target="_blank" rel="noopener">storytellers and creators</a> and <a href="https://www.lenovo.com/us/en/newrealities/" target="_blank" rel="noopener">raises their voices</a> while bringing rich experiences into consumers’ hands, from gaming to movies.</p>
                        <br>
                        <a href="https://www.lenovo.com/us/en/devices/" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-center">
                        <h3 class="text-white transat-text-standard">
                            Infrastructure
                        </h3>
                    </div>
                    <div class="text-white pr-1 pt-3">
                        <p class="font-14px">Powerful data centers, IoT integrations, AI-driven optimizations, and 5G networks will <a href="https://www.lenovoxperience.com/newsDetail/283yi044hzgcdv7snkrmmx9ozuct3m8frj9eqsrph5zy4lu8" target="_blank" rel="noopener">future-proof cities</a>, from orchestrating traffic to <a href="https://news.lenovo.com/5g-propels-the-future-of-an-ancient-chinese-town/" target="_blank" rel="noopener">autonomously cleaning busy streets</a>.</p>
                        <br>
                        <a  href="https://www.lenovo.com/us/en/data-center/" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-center">
                        <h3 class="text-white transat-text-standard">
                            Community
                        </h3>
                    </div>
                    <div class="text-white pr-1 pt-3">
                        <p class="font-14px">Through our Foundation, Lenovo directly fuels diverse futures in STEM, provides focused disaster relief, and helps employees volunteer, donate, and give back.</p>
                        <br>
                        <a href="https://www.lenovo.com/us/en/about/foundation/" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                    </div>
                </div>
                <div class="col-md-3 line-height-25px pt-lg-0 pt-md-0 pt-5">
                    <div class="text-center">
                        <h3 class="text-white transat-text-standard">
                            Sustainability
                        </h3>
                    </div>
                    <div class="text-white pr-1 pt-3">
                        <p class="font-14px">Our experts use AI and <a href="https://www.lenovo.com/us/en/data-center/neptune" target="_blank" rel="noopener">liquid-cooled supercomputers</a> to make <a href="https://news.lenovo.com/protecting-our-future-food-supply-with-ai-and-geospatial-analytics/" target="_blank" rel="noopener">agriculture smarter</a>, predict the effects of <a href="https://news.lenovo.com/supercomputing-ai-years-ahead-of-extreme-weather/" target="_blank" rel="noopener">changing weather</a>, and <a href="https://news.lenovo.com/our-forests-are-in-trouble-ai-can-help-bytelake/" target="_blank" rel="noopener">protect our forests</a>. &nbsp;And we made <a href="https://news.lenovo.com/path-to-a-brighter-more-sustainable-future/" target="_blank" rel="noopener">bold commitments</a> for our own operations to lead the way to a brighter, more sustainable future.</p>
                        <br>
                        <a href="https://investor.lenovo.com/en/sustainability/sustainability_reports.php" target="_blank" rel="noopener" class="text-white text-underline font-14px">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
