<?php
/**
* Template Name: Page FAQs
*
* Template for Home
*
* @package UnderStrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );?>
<div class="wrapper p-0 faqs-hero">
	<div class="container text-center position-relative title-header-section">
		<h1 class="text-white position-absolute center text-uppercase header-title"> FAQ </h1>
		<div class="scroll-more position-absolute">
            <div class="d-flex justify-content-center">
                <lottie-player src="<?php echo get_template_directory_uri(); ?>-child/js/data.json"  background="transparent"  speed="1"  loop autoplay></lottie-player>
            </div>
			<div class="pt-3 scroll-more-text transat-text-black text-white"> Scroll to <br> explore more </div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="w-100" style="height: 61px"></div>
		</div>
		<div class="row mt-5 justify-content-center next-content-to-scroll">
            <div class="col-lg-8 col-md-8">
                <h4 class="transat-text-standard text-white">
                    Island Conservation
                </h4>
                <div id="accordion">
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Why did Lenovo partner with Island Conservation on this project?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    What role does the partnership serve within the campaign?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                The partnership with Island Conservation is crucial to the effective implementation of Work for Humankind. They have the expertise, knowledge and access to Robinson Crusoe Island to ensure Work for Humankind has a positive and beneficial impact on the eco-system and residents of Robinson Crusoe Island.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    How will Lenovo be supporting Island Conservation with this campaign? And beyond?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<div class="row mt-5 justify-content-center">
            <div class="col-lg-8 col-md-8">
                <h4 class="transat-text-standard text-white">
                    Sophia Bush
                </h4>
                <div id="accordion">
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Why did Lenovo partner with Sophia?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    What is Sophia’s role within Work for Humankind?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingSix">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    What was Sophia compensated for her role within Work for Humankind?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<div class="row mt-5 justify-content-center">
            <div class="col-lg-8 col-md-8">
                <h4 class="transat-text-standard text-white">
                    Work for Humankind Research
                </h4>
                <div id="accordion">
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingSeven">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    Who commissioned the Lenovo global research?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingEight">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                    Why did Lenovo commission this piece of research?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingNine">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                    What methodology did the research providers use for this piece of research?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="headingTen">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                    How many people were surveyed for this research study?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="heading11">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                    What countries were surveyed as part of this research study?
                                </button>
                            </h5>
                        </div>
                        <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div class="card transat-text-medium">
                        <div class="card-header position-relative" id="heading12">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed font-20px" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                    How will Lenovo use this data and findings?
                                </button>
                            </h5>
                        </div>
                        <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                            <div class="card-body pl-5 pt-1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
