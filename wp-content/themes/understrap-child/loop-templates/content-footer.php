<?php
/**
* The template part to duplicates footers .
*
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package UnderStrap Child
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
$container   = get_theme_mod( 'understrap_container_type' );?>
<hr class="border-bottom position-relative border-1 m-0">

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-2 p-0 d-flex justify-content-lg-around justify-content-md-around justify-content-center align-items-center text-white">
                <div class="font-13px pr-lg-0 pr-md-0 pr-4">
                    Powered by
                </div>
                <div class="footer-logo">
                    <img src="<?php echo get_template_directory_uri(); ?>-child/images/Lenovo-Logo.png" alt="lenovo-logo">
                </div>
            </div>
            <div class="col-md-7 d-flex align-items-center">
                <div class="text-white font-9px pl-lg-4 pl-md-4 pl-0 pt-lg-0 pt-md-0 pt-3 text-center-mobile">
                    Lenovo uses cookies to understand how you use our website, improve your experience, and offer you personalized advertisements. <br class="mobile-hide">
                    Visit our Cookie Consent Tool to manage your preferences, and visit our privacy policy for more information.
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center align-items-lg-end align-items-md-end align-items-center flex-column text-white font-11px">
                <div class="pt-lg-0 pt-md-0 pt-3 text-center-mobile">
                    2021 Lenovo. All Rights Reserved
                </div>
                <div class="pt-lg-0 pt-md-0 pt-3 text-center-mobile">
                    <a target="_blank" href="https://www.lenovo.com/gb/en/privacy/" class="text-white">
                        Privacy
                    </a>
                    |
                    <a href="javascript:void(0)" class="text-white wt-cli-manage-consent-link">
                        Cookie Consent
                    </a>
                    |
                    <a target="_blank" href="https://www.lenovo.com/gb/en/legal/" class="text-white">
                        Terms of Use
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
