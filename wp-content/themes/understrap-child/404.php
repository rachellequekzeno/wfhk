<?php
/**
* The template for displaying 404 pages (not found).
*
* @package understrap
*/

get_header();

$container   = get_theme_mod( 'understrap_container_type' ); ?>

<div class="wrapper d-flex align-items-center pt-5 pb-5" id="error-404-wrapper">
	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<div class="content-area" id="primary">
			<main class="site-main" id="main">
				<section class="error-404 not-found">
					<div class="row">
						<div class="col-lg-8 offset-lg-2 col-12">
							<header class="page-header">
								<h1 class="page-title text-center">ERROR 404 <br class="d-block d-lg-none"/><span>NOT FOUND.</span></h1>
							</header><!-- .page-header -->

							<div class="page-content text-center">
								<p>Oops! That page can&rsquo;t be found. It looks like nothing was found at this location.</p>
								<?php get_search_form(); ?>
							</div><!-- .page-content -->
						</div><!-- .col-12 -->
					</div><!-- .row -->
				</section><!-- .error-404 -->
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->
<?php get_footer(); ?>
