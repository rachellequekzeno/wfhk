jQuery.noConflict();

// jQuery(window).load(function(){

//==================================================
// Homepage text fade in on scroll
// var contentWayPoint = function() {
//     var i = 0;
//     jQuery('.animate-box').waypoint( function( direction ) {
//         if( direction === 'down' && !jQuery(this.element).hasClass('animated') ) {
//             i++;
//             jQuery(this.element).addClass('item-animate');
//             setTimeout(function(){
//                 jQuery('body .animate-box.item-animate').each(function(k){
//                     var el = jQuery(this);
//                     setTimeout( function () {
//                         el.addClass('fadeInDown animated');
//                         el.removeClass('item-animate');
//                     },  k * 1000, 'easeInOutExpo' );
//                 });
//             }, 500);
//         }
//     } , { offset: '95%' } );
// };
// contentWayPoint();

// });

var last_scroll_top = 0;
jQuery(window).on('scroll', function() {
    scroll_top = jQuery(this).scrollTop();
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        if(scroll_top < last_scroll_top) {
            jQuery('.navbar-scroll').removeClass('scrolled-down').addClass('scrolled-up');
        } else {
            jQuery('.navbar-scroll').removeClass('scrolled-up').addClass('scrolled-down');
        }
    } else {
        jQuery('.navbar-scroll').removeClass('scrolled-down').removeClass('scrolled-up');
    }
    last_scroll_top = scroll_top;
});

jQuery(document).ready(function(){
    jQuery('.island-update-hero .collapse').on('shown.bs.collapse',function(){
        jQuery(this).parent().addClass('opened');
        jQuery('.card.opened .card-header .btn').html('Show less');
    });

    jQuery('.island-update-hero .collapse').on('hidden.bs.collapse',function(){
        jQuery('.card.opened .card-header .btn').html('Show more');
        jQuery(this).parent().removeClass('opened');
    });

    jQuery(".scroll-more").click(function() {
        if (jQuery(".next-content-to-scroll").length > 0) {
            jQuery('html, body').animate({
                scrollTop: jQuery(".next-content-to-scroll").offset().top
            }, 'slow');
        }
    });

    if (jQuery('body.home').length > 0){
        jQuery('.home #wrapper-navbar').addClass('loading-bay');

        gsap.registerPlugin(ScrollTrigger, ScrollToPlugin);

        let sections = gsap.utils.toArray("section"),
        currentSection = sections[0];
        const boxes = gsap.utils.toArray('.box');

        gsap.defaults({overwrite: 'auto', duration: 0.3});

        setTimeout(function() {
            // stretch out the body height according to however many sections there are.
            gsap.set("body", {
                height: (sections.length * 100) + "%"
            });

            // -- Remove the loading background.
            jQuery('.home #wrapper-navbar').removeClass('loading-bay');
            jQuery('.full-loading-bg').fadeOut(500);

            setTimeout(function() {
                jQuery('.full-loading-bg').remove();
            }, 3000);

            // -- Refresh the DOM so that the video will play back when revisit.
            ScrollTrigger.clearScrollMemory();
            ScrollTrigger.refresh();
        }, 3000);

        // -- Array of sections height with a default value.
        var arrayHeightSections = [
            0
        ];

        // -- Loop through all sections to get all the height.
        sections.forEach(function(sectionElement, sectionIndex) {
            arrayHeightSections.push((sectionIndex + 1) * innerHeight);
        });

        // -- Assign new data attributes to the navigation items.
        jQuery('.slider-navigation button').each(function(buttonIndex) {
            jQuery(this).attr('data-section-height', arrayHeightSections[buttonIndex]);

            // -- Adding the height break points to work with fading words.
            jQuery('#section' + (buttonIndex + 1)).attr('data-wording-section', arrayHeightSections[buttonIndex]);
        });

        // create a ScrollTrigger for each section
        sections.forEach((section, i) => {
            let videoElem = section.querySelector('video'),
            textingbox = section.querySelector('.texting-word'),
            spananimation = section.querySelector('span');

            const anim = gsap.fromTo(textingbox, {autoAlpha: 0, y: 50}, {duration: 1, autoAlpha: 1, y: 0});

            ScrollTrigger.create({
                // use dynamic scroll positions based on the window height (offset by half to make it feel natural)
                start: () => (i - 0.5) * innerHeight,
                end: () => (i + 0.5) * innerHeight  ,
                // when a new section activates (from either direction), set the section accordinglyl.
                trigger: videoElem,
                animation: anim,
                onEnter: () => videoElem.play(),
                onEnterBack: () => videoElem.play(),
                onLeave: () => videoElem.pause(),
                onLeaveBack: () => videoElem.pause(),
                onToggle: self => self.isActive && setSection(section, (i + 1) * innerHeight, innerHeight) // -- setSection is used to add active to the buttons.
            });

            ScrollTrigger.create({
                snap: 1 / 4 // snap whole page to the closest section!
            });
        });

        // -- Detect on click at the slider navigation button.
        jQuery('nav.slider-navigation').on('click', 'button', function () {
            // -- Get the height that been set to be scrolled.
            var currentHeightScroll = jQuery(this).data('section-height');

            // -- Get the section index number.
            var currentSectionIndexNumber = jQuery(this).data('change-to-section');

            clickSetSection(currentHeightScroll, currentSectionIndexNumber);
        });

        // -- Push to next section for scroll more icon.
        jQuery('body.home').on('click', '.scroll-more', function() {
            // -- Get the next button to activates.
            var nextButton = jQuery('nav.slider-navigation button.activates').next();

            clickSetSection(nextButton.data('section-height'), nextButton.data('change-to-section'));
        });

        function clickSetSection(heightScroll, sectionIndexNumber) {
            // -- Scroll to the assigned height.
            jQuery('html, body').animate({
                scrollTop: heightScroll
            }, 'fast');

            // -- Define the new section based on index number.
            newSection = sections[sectionIndexNumber];

            // -- Set the currentSection to be the newSection.
            setSection(newSection);
        }

        // -- Used to update section and add active class.
        function setSection(newSection, currentPositionSection, additionValue) {
            // -- Check if the new section matches with current section.
            if (newSection !== currentSection) {
                // -- Transition to other sections.
                gsap.to(currentSection, { autoAlpha: 0 })
                gsap.to(newSection, { autoAlpha: 1 });

                // -- Reset the currentSection value to the newSection.
                currentSection = newSection;

                // -- Refresh the DOM so that the video will play back when revisit.
                ScrollTrigger.clearScrollMemory();
                ScrollTrigger.refresh();

                // -- Used to define which data-section-height. By default is 0.
                var activateButtonValue = 0;

                // -- Check if the current height is equals to the additionValue. Which is the starting section.
                if (currentPositionSection != additionValue) {
                    activateButtonValue = currentPositionSection - additionValue;
                }

                // -- Remove all activates class from the navigation buttons.
                jQuery('nav.slider-navigation button').removeClass('activates');

                // -- Get the current position to activate the correct navigation button.
                jQuery('nav.slider-navigation button[data-section-height="' + activateButtonValue + '"]').addClass('activates');

                wordFading(activateButtonValue, 0);
            }
        }

        function wordFading(heightValue, currentIndex) {
            if (currentIndex == 0) {
                // -- Fade element based on order.
                jQuery('.panel[data-wording-section="' + heightValue + '"] .wording-fade[data-wording-fade-order="' + currentIndex + '"]').addClass('animated fadeIn');

                // -- Add on 1 to currentIndex value.
                currentIndex++;

                // -- Run function recursive untill condition passed.
                wordFading(heightValue, currentIndex);
            } else {
                setTimeout(function() {
                    // -- Check if current index is smaller than total number of element exists.
                    if(currentIndex < jQuery('.panel[data-wording-section="' + heightValue + '"] .wording-fade').length) {
                        // -- Fade element based on order.
                        jQuery('.panel[data-wording-section="' + heightValue + '"] .wording-fade[data-wording-fade-order="' + currentIndex + '"]').addClass('animated fadeIn');

                        // -- Add on 1 to currentIndex value.
                        currentIndex++;

                        // -- Run function recursive untill condition passed.
                        wordFading(heightValue, currentIndex);
                    }
                }, 1000); // -- Set timeout for 1 second
            }
        }
    }

    jQuery(".navbar-toggler").click(function(e) {
        e.preventDefault();
        jQuery("#main-nav").toggleClass("expanded-mobile-menu");
        jQuery(".logo-header img").toggleClass('opacity');
        jQuery("#menu-fade").toggleClass("addclass");
        jQuery(".wrapper").toggleClass("hide-behind-menu");
        jQuery(this).toggleClass('expanded');

        if (jQuery("body").hasClass("compensate-for-scrollbar")) {
            setTimeout(function() {
                jQuery("body").removeClass("compensate-for-scrollbar");
            }, 500);
        } else {
            jQuery("body").addClass("compensate-for-scrollbar")
        }
    });


    jQuery('.form-skills').on("click",function(){
        var customCheckBox = jQuery(this).data('skills');
        jQuery(this).addClass('active');
        jQuery('.form-noskills').removeClass('active');
        jQuery('input[value="'+ customCheckBox+'"]').prop('checked', true);
        jQuery('input[value="None above"]').prop('checked', false);
    });

    jQuery('.form-noskills').on("click",function(){
        jQuery('input[name="skills[]"]').prop('checked', false);
        jQuery('input[value="None above"]').prop('checked', true);
        jQuery('.form-skills').removeClass('active');
        jQuery(this).addClass('active');
    });

    jQuery('.form-role').on("click",function(){
        var customRadioBox = jQuery(this).data('role');
        jQuery('input[name="role"]').prop('checked', false);
        jQuery('.role input[value="'+ customRadioBox+'"]').prop('checked', true);
        jQuery('.form-role').removeClass('active');
        jQuery(this).addClass('active');
    });

    jQuery("span.wpcf7-form-control-wrap textarea.charlimit").on('keyup', function() {
        var words = this.value.match(/\S+/g).length;

        if (words > 200) {
            // Split the string on first 200 words and rejoin on spaces
            var trimmed = jQuery(this).val().split(/\s+/, 200).join(" ");
            // Add a space at the end to make sure more typing creates new words
            jQuery(this).val(trimmed + " ");
        }
        else {
            // jQuery(this).text(words);
            // var newword = 200-words;
            jQuery(this).parents().find('.word_left').text('('+ words + '/200 words)'  );
        }
    });

});

// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});
