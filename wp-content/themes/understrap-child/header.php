<?php
/**
* The header for our theme
*
* Displays all of the <head> section and everything up till <div id="content">
*
* @package Understrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
	<?php do_action( 'wp_body_open' );
	global $post;?>
	<div class="site" id="page">

		<!-- ******************* The Navbar Area ******************* -->

		<div id="wrapper-navbar" class="w-100 <?php if (is_front_page()) { echo 'position-fixed'; } else if ($post->ID == 19) { echo 'position-absolute'; } else { echo 'navbar-scroll'; }?> linear-menu-background">
			<nav id="main-nav" class="position-relative navbar navbar-expand-lg navbar-light" aria-labelledby="main-nav-label">
				<div class="w-100 d-flex position-relative align-items-center flex-wrap-mobile">
					<a class="navbar-brand m-0" rel="home" href="<?php bloginfo('url'); ?>" itemprop="url">
						<img class="wfhk-logo px-3" src="<?php echo get_template_directory_uri(); ?>-child/images/logo.png" alt="">
					</a>
					<a href="#menu" class="navbar-toggler white position-relative ml-auto my-auto mobile-show">
						<div class="bar bar1"></div>
						<div class="bar bar2"></div>
						<div class="bar bar3"></div>
					</a>
					<div id="navbarNavDropdown" class="collapse navbar-collapse justify-content-around">
						<ul id="main-menu" class="navbar-nav w-100 align-items-center">
							<li class="nav-item <?php if( $post->ID == 8) echo 'active'; ?>">
								<a href="<?php bloginfo('url'); ?>/island-update" class="nav-link p-lg-4">Island Update</a>
							</li>
							<li class="nav-item <?php if( $post->ID == 6) echo 'active'; ?>">
								<a href="<?php bloginfo('url'); ?>/island-conservation" class="nav-link p-lg-4">Island Conservation</a>
							</li>
							<li class="nav-item <?php if( $post->ID == 10) echo 'active'; ?>">
								<a href="<?php bloginfo('url'); ?>/the-community" class="nav-link p-lg-4">The Community</a>
							</li>
							<li class="nav-item <?php if( $post->ID == 12) echo 'active'; ?>">
								<a href="<?php bloginfo('url'); ?>/the-workspace" class="nav-link p-lg-4">The Workspace</a>
							</li>
							<li class="nav-item <?php if( $post->ID == 15) echo 'active'; ?>">
								<a href="<?php bloginfo('url'); ?>/lenovo-solutions" class="nav-link p-lg-4">Lenovo Solutions</a>
							</li>
							<li class="nav-item <?php if( $post->ID == 17) echo 'active'; ?>">
								<a href="<?php bloginfo('url'); ?>/faqs" class="nav-link py-4 px-4">FAQs</a>
							</li>
							<li class="btn nav-item px-4 rounded-0 ml-auto">
								<a href="<?php bloginfo('url'); ?>/apply-now" class="nav-link">Apply Now</a>
							</li>
						</ul>
					</div>

					<div class="menu menu--fade" id="menu-fade">
						<div class="menu__content d-flex align-items-center justify-content-center h-100">
							<div class="menu-links-content position-relative text-center w-100 h-100">
								<div class="menu-links d-flex flex-column justify-content-xl-start justify-content-center">
									<a class="menu-link mb-2 text-decoration-none <?php if( $post->ID == 8) echo 'current'; ?>" href="<?php bloginfo('url'); ?>/island-update">
										<span class="link-text mb-4">Island Update</span>
									</a>
									<a class="menu-link mb-2 text-decoration-none <?php if( $post->ID == 6) echo 'current'; ?>" href="<?php bloginfo('url'); ?>/island-conservation">
										<span class="link-text mb-4">Island Conservation</span>
									</a>
									<a class="menu-link mb-2 text-decoration-none <?php if( $post->ID == 10) echo 'current'; ?>" href="<?php bloginfo('url'); ?>/the-community">
										<span class="link-text mb-4">The Community</span>
									</a>
									<a class="menu-link mb-2 text-decoration-none <?php if( $post->ID == 12) echo 'current'; ?>" href="<?php bloginfo('url'); ?>/the-workspace">
										<span class="link-text mb-4">The Workspace</span>
									</a>
									<a class="menu-link mb-2 text-decoration-none <?php if( $post->ID == 15) echo 'current'; ?>" href="<?php bloginfo('url'); ?>/lenovo-solutions">
										<span class="link-text mb-4">Lenovo Solutions</span>
									</a>
									<a class="menu-link mb-2 text-decoration-none <?php if( $post->ID == 17) echo 'current'; ?>" href="<?php bloginfo('url'); ?>/faqs">
										<span class="link-text mb-4">FAQs</span>
									</a>
									<a class="menu-link mb-2 text-decoration-none <?php if( $post->ID == 19) echo 'current'; ?>" href="<?php bloginfo('url'); ?>/apply-now">
										<span class="link-text mb-4">Apply Now</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</nav>

			<hr class="border-bottom border-1 m-0">
		</div>
