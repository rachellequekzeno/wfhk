<?php
function understrap_remove_scripts() {
  wp_dequeue_style( 'understrap-styles' );
  wp_deregister_style( 'understrap-styles' );

  wp_dequeue_script( 'understrap-scripts' );
  wp_deregister_script( 'understrap-scripts' );
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

  // Get the theme data
  $the_theme = wp_get_theme();
  wp_enqueue_style( 'animation-style', get_stylesheet_directory_uri() . '/css/jquery.animation.css' );
  wp_enqueue_style( 'fullpage-styles', get_stylesheet_directory_uri() . '/css/fullpage.min.css', array(), $the_theme->get( 'Version' ) );
  wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.css', array(), $the_theme->get( 'Version' ) );
  wp_enqueue_style( 'custom-theme', get_stylesheet_directory_uri() . '/css/custom-theme.css', array(), $the_theme->get( 'Version' ) );
  wp_enqueue_style( 'custom-css', get_stylesheet_directory_uri() . '/css/custom-css.css', array(), $the_theme->get( 'Version' ) );

  wp_enqueue_script( 'jquery');
  wp_enqueue_script( 'popper-scripts', get_stylesheet_directory_uri() . '/js/popper.min.js', array(), false);
  // wp_enqueue_script( 'fullpage-scripts', get_stylesheet_directory_uri() . '/js/fullpage.min.js' );
  wp_enqueue_script( 'waypoints-scripts', get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js' );
  wp_enqueue_script( 'lottie-player', get_stylesheet_directory_uri() . '/js/lottie-player.js', array(), $the_theme->get( 'Version' ), true );
  wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.js', array(), $the_theme->get( 'Version' ), true );
  wp_enqueue_script( 'index-scripts', get_stylesheet_directory_uri() . '/js/index.js', array(), $the_theme->get( 'Version' ), false );
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}

function add_child_theme_textdomain() {
  load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );
