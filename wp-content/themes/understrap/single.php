<?php
/**
* The template for displaying all single posts
*
* @package Understrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper h-100" id="full-width-page-wrapper">
	<div class="h-100" id="content">
		<div class="content-area h-100" id="primary">
			<main class="site-main h-100" id="main" role="main">
				<div id="fullpage">
					<div class="section " id="section1">
						<video id="myVideo" loop muted data-autoplay>
							<source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-1.mp4" type="video/mp4">
							</video>
							<div class="layer">
								<h1>fullPage.js videos</h1>
							</div>
						</div>
						<div class="section" id="section2">
							<video id="myOtherVideo" loop muted data-autoplay>
								<source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-2.mp4" type="video/mp4">
								</video>
								<div class="layer">
									<h1>fullPage.js videos</h1>
								</div>
							</div>
							<div class="section" id="section3">
								<video id="myOtherVideo" loop muted data-autoplay>
									<source src="<?php echo get_template_directory_uri(); ?>-child/assets/home-chapter-2.mp4" type="video/mp4">
									</video>
									<div class="layer">
										<h1>fullPage.js videos</h1>
									</div>
								</div>
							</div>
						</main><!-- #main -->
					</div><!-- #primary -->
				</div><!-- #content -->
			</div><!-- #full-width-page-wrapper -->

			<?php
			get_footer();
